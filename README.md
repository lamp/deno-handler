- use with web file server as handler for js/ts files
- web server i.e. caddy serves static files and directory indexes
- js/ts files sent to deno like how php is sent to php-fpm
- quick & easy server-side js
- still not sure what to call it

# deno-handler.ts
```ts
import { serve } from "https://deno.land/std@0.139.0/http/server.ts";

serve(async function handler(req: Request): Promise<Response> {

	var url = new URL(req.url);
	var webroot = req.headers.get("webroot");
	var fullpath = webroot + url.pathname;

	if (!webroot || fullpath.includes('../') || !/\.(js|ts)$/i.test(url.pathname)) return new Response("", {status: 400});

	try {
		var {handler} = await import(fullpath);
		return await handler(req);
	} catch (error) {
		return new Response(error.message, {
			status: error.code == "ERR_MODULE_NOT_FOUND" ? 404 : 500
		});
	}
	
}, {
	port: 8000,
	hostname: "localhost"
});
```

# example caddy config
```caddyfile
example.com {
		root * /var/www/html/

		try_files {path} {path}/index.js {path}/index.ts

		@serverjs {
				path *.js *.ts
				not path /static/* #in case you want to serve client js
				#or another idea is to match *.server.js/ts
		}
		reverse_proxy @serverjs localhost:8000 {
				header_up webroot /var/www/html/ #need to tell deno server where files are
		}

		file_server browse
}
```

# example systemd service
```systemd
[Unit]
Description=thing for running server-side js files in a webroot
After=network.target

[Service]
# user with home dir needed for webstorage/cache
User=deno
Group=deno
WorkingDirectory=/home/deno
ExecStart=deno run --allow-all deno-handler.ts

[Install]
WantedBy=multi-user.target
```

# example files
```js
export function handler(req) {
	return new Response("foobar javascript");
}
```
```ts
export function handler(req: Request): Response {
	return new Response("foobar typescript");
}
```
```ts
export async function handler(req: Request): Promise<Response> {
	await new Promise(r => setTimeout(r, 3000));
	return new Response("foobar async typescript");
}
```
note that edits or deletes to modules require restarting deno